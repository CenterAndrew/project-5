Player = new Mongo.Collection('player');

Player.attachSchema(new SimpleSchema({

  userId:{
    type:String
  }

}));
/*
 * Add query methods like this:
 *  Player.findPublic = function () {
 *    return Player.find({is_public: true});
 *  }
 */